// Import the page's CSS. Webpack will know what to do with it.
import "../styles/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

/*
 * When you compile and deploy your Voting contract,
 * truffle stores the abi and deployed address in a json
 * file in the build directory. We will use this information
 * to setup a Voting abstraction. We will use this abstraction
 * later to create an instance of the Voting contract.
 * Compare this against the index.js from our previous tutorial to see the difference
 * https://gist.github.com/maheshmurthy/f6e96d6b3fff4cd4fa7f892de8a1a1b4#file-index-js
 */

import voting_artifacts from '../../build/contracts/Voting.json'

var Voting = contract(voting_artifacts);

//empty list of candidates
let candidates = {}

//no token price is set
let tokenPrice = null;

//triggers on vote button
window.voteForCandidate = function(candidate) {
	//get the values in the candidate and vote-tokens id's and store them
  let candidateName = $("#candidate").val();
  let voteTokens = $("#vote-tokens").val();
  //alert user with vote submission
  $("#msg").html("Vote has been submitted. The vote count will increment as soon as the vote is recorded on the blockchain. Please wait.")
  //empty input boxes after submission
  $("#candidate").val("");
  $("#vote-tokens").val("");

  /* Voting.deployed() returns an instance of the contract. Every call
   * in Truffle returns a promise which is why we have used then()
   * everywhere we have a transaction call
   */
  Voting.deployed().then(function(contractInstance) {
  //call the contract function voteForCandidate, with the candidate name number of tokens
  //note all transactions are being performed by user 0 by default
  //this function spends gas, since it changes state of blockchain
    contractInstance.voteForCandidate(candidateName, voteTokens, {gas: 140000, from: web3.eth.accounts[0]}).then(function() {
      //get the total number of votes for the candidate you just voted for and update his ui field with the new number of votes
      //this function call doesn't spend gas due to - call(), and it doesn't change blockchain state
      let div_id = candidates[candidateName];
      return contractInstance.totalVotesFor.call(candidateName).then(function(v) {
        $("#" + div_id).html(v.toString());
        $("#msg").html("");
      });
    });
  });
}

/* The user enters the total no. of tokens to buy. We calculate the total cost and send it in
 * the request. We have to send the value in Wei. So, we use the toWei helper method to convert
 * from Ether to Wei.
 */

window.buyTokens = function() {
	//get the amount of tokens to buy from the buy id field
  let tokensToBuy = $("#buy").val();
  //calculate the token price
  let price = tokensToBuy * tokenPrice;
  $("#buy-msg").html("Purchase order has been submitted. Please wait.");
  Voting.deployed().then(function(contractInstance) {
  	// call smart contract function and transfer the total amount of wei to the smart contract 
    contractInstance.buy({value: web3.toWei(price, 'ether'), from: web3.eth.accounts[0]}).then(function(v) {
      //reset previous buy msg
      $("#buy-msg").html("");
      //use web3 api call to get current smart contract balance and update the appropriate field with the new ether balance recieved
      web3.eth.getBalance(contractInstance.address, function(error, result) {
        $("#contract-balance").html(web3.fromWei(result.toString()) + " Ether");
      });
    })
  });
  
  //get the token data and update the appropriate fields
  populateTokenData();
}


//function cast upon look up voter info button is pressed

window.lookupVoterInfo = function() {
	//take the address supplied by the user in the appropriate field
  let address = $("#voter-info").val();
  Voting.deployed().then(function(contractInstance) {
  	//call the contract function voterDetails, with the desired address - no gas is spent 
  	//this function will return the number of tokens bought and an array of how the tokens
  	//were spent on each candidate as votes
    contractInstance.voterDetails.call(address).then(function(v) {
    	//displays the tokens bought and iterates through the array returend to display how votes were cast
      $("#tokens-bought").html("Total Tokens bought: " + v[0].toString());
      let votesPerCandidate = v[1];
      $("#votes-cast").empty();
      $("#votes-cast").append("Votes cast per candidate: <br>");
      let allCandidates = Object.keys(candidates);
      for(let i=0; i < allCandidates.length; i++) {
        $("#votes-cast").append(allCandidates[i] + ": " + votesPerCandidate[i] + "<br>");
      }
    });
  });
}

/* Instead of hardcoding the candidates hash, we now fetch the candidate list from
 * the blockchain and populate the array. Once we fetch the candidates, we setup the
 * table in the UI with all the candidates and the votes they have received.
 */
function populateCandidates() {
  Voting.deployed().then(function(contractInstance) {
  	// use smart contract function to get all candidates without spending gas -  function returns array
    contractInstance.allCandidates.call().then(function(candidateArray) {
      for(let i=0; i < candidateArray.length; i++) {
        /* We store the candidate names as bytes32 on the blockchain. We use the
         * handy toUtf8 method from web3 library to convert from bytes32 to string
         */
        candidates[web3.toUtf8(candidateArray[i])] = "candidate-" + i;
      }
      
      //insert html section with rows for each candidate
      setupCandidateRows();
      
      // update how many votes each candidate has
      populateCandidateVotes();
      
      //update how many tokens are available for sale and the price of each
      populateTokenData();
    });
  });
}


//update ui with number of votes per candidate
function populateCandidateVotes() {
	//get the names to candidate number mapping we created in pupulateCandidates, and store the names in candidateNames
  let candidateNames = Object.keys(candidates);
  for (var i = 0; i < candidateNames.length; i++) {
    let name = candidateNames[i];
    //for each candidate, use his name as a parameter to get his number of votes from the blockchain - w/o spending gas
    Voting.deployed().then(function(contractInstance) {
      contractInstance.totalVotesFor.call(name).then(function(v) {
      	//get the number of votes per candidate and display them in the ui, using the candidate number (mapped to his name)
      	//as an id
        $("#" + candidates[name]).html(v.toString());
      });
    });
  }
}

//set up the rows for each candidate in the ui using html injection
function setupCandidateRows() {
  Object.keys(candidates).forEach(function (candidate) { 
    $("#candidate-rows").append("<tr><td>" + candidate + "</td><td id='" + candidates[candidate] + "'></td></tr>");
  });
}

/* Fetch the total tokens, tokens available for sale and the price of
 * each token and display in the UI
 */
function populateTokenData() {
	/*Note that totalTokens and tokenPrice are state variables that are set to public in the smart contract.
	 *This means that the compiler automatically creates an accessor function which enables us to use it in the
	 *js abi to access that value. Note that this accessor function will not be consuming gas.
	 */

  Voting.deployed().then(function(contractInstance) {
    contractInstance.totalTokens().then(function(v) {
      $("#tokens-total").html(v.toString());
    });
    
    //tokensSold is a function in oour smart contract that returns the amount of tokens sold. This also won't be
    //consimuing gas since it is used with the call()
    contractInstance.tokensSold.call().then(function(v) {
      $("#tokens-sold").html(v.toString());
    });
    contractInstance.tokenPrice().then(function(v) {
      tokenPrice = parseFloat(web3.fromWei(v.toString()));
      $("#token-cost").html(tokenPrice + " Ether");
    });
    
    //web3 api used to get balance of an address at a given block, in this case it is the default block, however
    //with a callback to update the current balance of the smart contract
    web3.eth.getBalance(contractInstance.address, function(error, result) {
    	//to display the appropriate amount of ether in the ui, the web3 api call fromWei is used.
    	//it converts a number of wei into another denomination of it eg. ether
      $("#contract-balance").html(web3.fromWei(result.toString()) + " Ether");
    });
  });
}

// first functions to get called, since they establish the web3 connection and make sure browser is web3 compatible
$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  Voting.setProvider(web3.currentProvider);
  
  //set up user interface, by fetching candidate list from blockchain
  populateCandidates();
  
});

/*Note - web3.eth.accounts returns is a web3 api call with a read only property and returns an array
 *of accounts that the node controls. If we use web3.eth.accounts[n], we are accessing the n'th element
 *of the returned array in one go
 */ 

