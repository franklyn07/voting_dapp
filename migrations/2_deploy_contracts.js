var Voting = artifacts.require("./Voting.sol");
module.exports = function(deployer) {
  deployer.deploy(Voting, 1000, web3.toWei('0.1', 'ether'), ['Rama', 'Nick', 'Jose']);
};

//migrate contract voting to the blockchain, with an initial 1000 tokens available
//with each token price being 0.1 units of ethereeum which will be converted to 1 ether
//the list of canidates with which the contract should be deployed is also passed as a parameter to the constructor

/*Note that web3 api call toWei is used to convert an ethereum unit into another
 *denomination, in this case being ether
 */
